package com.tintinonline.ecom.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.tintinonline.ecom.web.rest.TestUtil;

public class OrderProductLineDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderProductLineDTO.class);
        OrderProductLineDTO orderProductLineDTO1 = new OrderProductLineDTO();
        orderProductLineDTO1.setId(1L);
        OrderProductLineDTO orderProductLineDTO2 = new OrderProductLineDTO();
        assertThat(orderProductLineDTO1).isNotEqualTo(orderProductLineDTO2);
        orderProductLineDTO2.setId(orderProductLineDTO1.getId());
        assertThat(orderProductLineDTO1).isEqualTo(orderProductLineDTO2);
        orderProductLineDTO2.setId(2L);
        assertThat(orderProductLineDTO1).isNotEqualTo(orderProductLineDTO2);
        orderProductLineDTO1.setId(null);
        assertThat(orderProductLineDTO1).isNotEqualTo(orderProductLineDTO2);
    }
}
