package com.tintinonline.ecom.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.tintinonline.ecom.web.rest.TestUtil;

public class ProductUnitDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProductUnitDTO.class);
        ProductUnitDTO productUnitDTO1 = new ProductUnitDTO();
        productUnitDTO1.setId(1L);
        ProductUnitDTO productUnitDTO2 = new ProductUnitDTO();
        assertThat(productUnitDTO1).isNotEqualTo(productUnitDTO2);
        productUnitDTO2.setId(productUnitDTO1.getId());
        assertThat(productUnitDTO1).isEqualTo(productUnitDTO2);
        productUnitDTO2.setId(2L);
        assertThat(productUnitDTO1).isNotEqualTo(productUnitDTO2);
        productUnitDTO1.setId(null);
        assertThat(productUnitDTO1).isNotEqualTo(productUnitDTO2);
    }
}
