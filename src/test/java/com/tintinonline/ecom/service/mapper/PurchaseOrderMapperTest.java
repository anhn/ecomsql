package com.tintinonline.ecom.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class PurchaseOrderMapperTest {

    private PurchaseOrderMapper purchaseOrderMapper;

    @BeforeEach
    public void setUp() {
        purchaseOrderMapper = new PurchaseOrderMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(purchaseOrderMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(purchaseOrderMapper.fromId(null)).isNull();
    }
}
