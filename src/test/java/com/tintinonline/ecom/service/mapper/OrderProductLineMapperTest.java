package com.tintinonline.ecom.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OrderProductLineMapperTest {

    private OrderProductLineMapper orderProductLineMapper;

    @BeforeEach
    public void setUp() {
        orderProductLineMapper = new OrderProductLineMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(orderProductLineMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(orderProductLineMapper.fromId(null)).isNull();
    }
}
