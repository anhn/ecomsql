package com.tintinonline.ecom.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ProductUnitMapperTest {

    private ProductUnitMapper productUnitMapper;

    @BeforeEach
    public void setUp() {
        productUnitMapper = new ProductUnitMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(productUnitMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(productUnitMapper.fromId(null)).isNull();
    }
}
