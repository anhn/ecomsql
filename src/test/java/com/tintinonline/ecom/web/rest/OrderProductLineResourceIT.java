package com.tintinonline.ecom.web.rest;

import com.tintinonline.ecom.EcomsqlApp;
import com.tintinonline.ecom.domain.OrderProductLine;
import com.tintinonline.ecom.domain.Product;
import com.tintinonline.ecom.domain.PurchaseOrder;
import com.tintinonline.ecom.repository.OrderProductLineRepository;
import com.tintinonline.ecom.service.OrderProductLineService;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;
import com.tintinonline.ecom.service.mapper.OrderProductLineMapper;
import com.tintinonline.ecom.service.dto.OrderProductLineCriteria;
import com.tintinonline.ecom.service.OrderProductLineQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrderProductLineResource} REST controller.
 */
@SpringBootTest(classes = EcomsqlApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OrderProductLineResourceIT {

    private static final Double DEFAULT_QUANTITY = 1D;
    private static final Double UPDATED_QUANTITY = 2D;
    private static final Double SMALLER_QUANTITY = 1D - 1D;

    private static final Double DEFAULT_SUB_TOTAL = 1D;
    private static final Double UPDATED_SUB_TOTAL = 2D;
    private static final Double SMALLER_SUB_TOTAL = 1D - 1D;

    @Autowired
    private OrderProductLineRepository orderProductLineRepository;

    @Autowired
    private OrderProductLineMapper orderProductLineMapper;

    @Autowired
    private OrderProductLineService orderProductLineService;

    @Autowired
    private OrderProductLineQueryService orderProductLineQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOrderProductLineMockMvc;

    private OrderProductLine orderProductLine;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderProductLine createEntity(EntityManager em) {
        OrderProductLine orderProductLine = new OrderProductLine()
            .quantity(DEFAULT_QUANTITY)
            .subTotal(DEFAULT_SUB_TOTAL);
        // Add required entity
        Product product;
        if (TestUtil.findAll(em, Product.class).isEmpty()) {
            product = ProductResourceIT.createEntity(em);
            em.persist(product);
            em.flush();
        } else {
            product = TestUtil.findAll(em, Product.class).get(0);
        }
        orderProductLine.setProduct(product);
        return orderProductLine;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderProductLine createUpdatedEntity(EntityManager em) {
        OrderProductLine orderProductLine = new OrderProductLine()
            .quantity(UPDATED_QUANTITY)
            .subTotal(UPDATED_SUB_TOTAL);
        // Add required entity
        Product product;
        if (TestUtil.findAll(em, Product.class).isEmpty()) {
            product = ProductResourceIT.createUpdatedEntity(em);
            em.persist(product);
            em.flush();
        } else {
            product = TestUtil.findAll(em, Product.class).get(0);
        }
        orderProductLine.setProduct(product);
        return orderProductLine;
    }

    @BeforeEach
    public void initTest() {
        orderProductLine = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderProductLine() throws Exception {
        int databaseSizeBeforeCreate = orderProductLineRepository.findAll().size();
        // Create the OrderProductLine
        OrderProductLineDTO orderProductLineDTO = orderProductLineMapper.toDto(orderProductLine);
        restOrderProductLineMockMvc.perform(post("/api/order-product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProductLineDTO)))
            .andExpect(status().isCreated());

        // Validate the OrderProductLine in the database
        List<OrderProductLine> orderProductLineList = orderProductLineRepository.findAll();
        assertThat(orderProductLineList).hasSize(databaseSizeBeforeCreate + 1);
        OrderProductLine testOrderProductLine = orderProductLineList.get(orderProductLineList.size() - 1);
        assertThat(testOrderProductLine.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testOrderProductLine.getSubTotal()).isEqualTo(DEFAULT_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void createOrderProductLineWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderProductLineRepository.findAll().size();

        // Create the OrderProductLine with an existing ID
        orderProductLine.setId(1L);
        OrderProductLineDTO orderProductLineDTO = orderProductLineMapper.toDto(orderProductLine);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderProductLineMockMvc.perform(post("/api/order-product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProductLineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrderProductLine in the database
        List<OrderProductLine> orderProductLineList = orderProductLineRepository.findAll();
        assertThat(orderProductLineList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderProductLines() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderProductLine.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.doubleValue())))
            .andExpect(jsonPath("$.[*].subTotal").value(hasItem(DEFAULT_SUB_TOTAL.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getOrderProductLine() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get the orderProductLine
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines/{id}", orderProductLine.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(orderProductLine.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY.doubleValue()))
            .andExpect(jsonPath("$.subTotal").value(DEFAULT_SUB_TOTAL.doubleValue()));
    }


    @Test
    @Transactional
    public void getOrderProductLinesByIdFiltering() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        Long id = orderProductLine.getId();

        defaultOrderProductLineShouldBeFound("id.equals=" + id);
        defaultOrderProductLineShouldNotBeFound("id.notEquals=" + id);

        defaultOrderProductLineShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultOrderProductLineShouldNotBeFound("id.greaterThan=" + id);

        defaultOrderProductLineShouldBeFound("id.lessThanOrEqual=" + id);
        defaultOrderProductLineShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity equals to DEFAULT_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.equals=" + DEFAULT_QUANTITY);

        // Get all the orderProductLineList where quantity equals to UPDATED_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.equals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsNotEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity not equals to DEFAULT_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.notEquals=" + DEFAULT_QUANTITY);

        // Get all the orderProductLineList where quantity not equals to UPDATED_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.notEquals=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsInShouldWork() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity in DEFAULT_QUANTITY or UPDATED_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.in=" + DEFAULT_QUANTITY + "," + UPDATED_QUANTITY);

        // Get all the orderProductLineList where quantity equals to UPDATED_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.in=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity is not null
        defaultOrderProductLineShouldBeFound("quantity.specified=true");

        // Get all the orderProductLineList where quantity is null
        defaultOrderProductLineShouldNotBeFound("quantity.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity is greater than or equal to DEFAULT_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.greaterThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the orderProductLineList where quantity is greater than or equal to UPDATED_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.greaterThanOrEqual=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity is less than or equal to DEFAULT_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.lessThanOrEqual=" + DEFAULT_QUANTITY);

        // Get all the orderProductLineList where quantity is less than or equal to SMALLER_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.lessThanOrEqual=" + SMALLER_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsLessThanSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity is less than DEFAULT_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.lessThan=" + DEFAULT_QUANTITY);

        // Get all the orderProductLineList where quantity is less than UPDATED_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.lessThan=" + UPDATED_QUANTITY);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesByQuantityIsGreaterThanSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where quantity is greater than DEFAULT_QUANTITY
        defaultOrderProductLineShouldNotBeFound("quantity.greaterThan=" + DEFAULT_QUANTITY);

        // Get all the orderProductLineList where quantity is greater than SMALLER_QUANTITY
        defaultOrderProductLineShouldBeFound("quantity.greaterThan=" + SMALLER_QUANTITY);
    }


    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal equals to DEFAULT_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.equals=" + DEFAULT_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal equals to UPDATED_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.equals=" + UPDATED_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsNotEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal not equals to DEFAULT_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.notEquals=" + DEFAULT_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal not equals to UPDATED_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.notEquals=" + UPDATED_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsInShouldWork() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal in DEFAULT_SUB_TOTAL or UPDATED_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.in=" + DEFAULT_SUB_TOTAL + "," + UPDATED_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal equals to UPDATED_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.in=" + UPDATED_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsNullOrNotNull() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal is not null
        defaultOrderProductLineShouldBeFound("subTotal.specified=true");

        // Get all the orderProductLineList where subTotal is null
        defaultOrderProductLineShouldNotBeFound("subTotal.specified=false");
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal is greater than or equal to DEFAULT_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.greaterThanOrEqual=" + DEFAULT_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal is greater than or equal to UPDATED_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.greaterThanOrEqual=" + UPDATED_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal is less than or equal to DEFAULT_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.lessThanOrEqual=" + DEFAULT_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal is less than or equal to SMALLER_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.lessThanOrEqual=" + SMALLER_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsLessThanSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal is less than DEFAULT_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.lessThan=" + DEFAULT_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal is less than UPDATED_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.lessThan=" + UPDATED_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void getAllOrderProductLinesBySubTotalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        // Get all the orderProductLineList where subTotal is greater than DEFAULT_SUB_TOTAL
        defaultOrderProductLineShouldNotBeFound("subTotal.greaterThan=" + DEFAULT_SUB_TOTAL);

        // Get all the orderProductLineList where subTotal is greater than SMALLER_SUB_TOTAL
        defaultOrderProductLineShouldBeFound("subTotal.greaterThan=" + SMALLER_SUB_TOTAL);
    }


    @Test
    @Transactional
    public void getAllOrderProductLinesByProductIsEqualToSomething() throws Exception {
        // Get already existing entity
        Product product = orderProductLine.getProduct();
        orderProductLineRepository.saveAndFlush(orderProductLine);
        Long productId = product.getId();

        // Get all the orderProductLineList where product equals to productId
        defaultOrderProductLineShouldBeFound("productId.equals=" + productId);

        // Get all the orderProductLineList where product equals to productId + 1
        defaultOrderProductLineShouldNotBeFound("productId.equals=" + (productId + 1));
    }


    @Test
    @Transactional
    public void getAllOrderProductLinesByPurchaseOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);
        PurchaseOrder purchaseOrder = PurchaseOrderResourceIT.createEntity(em);
        em.persist(purchaseOrder);
        em.flush();
        orderProductLine.setPurchaseOrder(purchaseOrder);
        orderProductLineRepository.saveAndFlush(orderProductLine);
        Long purchaseOrderId = purchaseOrder.getId();

        // Get all the orderProductLineList where purchaseOrder equals to purchaseOrderId
        defaultOrderProductLineShouldBeFound("purchaseOrderId.equals=" + purchaseOrderId);

        // Get all the orderProductLineList where purchaseOrder equals to purchaseOrderId + 1
        defaultOrderProductLineShouldNotBeFound("purchaseOrderId.equals=" + (purchaseOrderId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultOrderProductLineShouldBeFound(String filter) throws Exception {
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderProductLine.getId().intValue())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.doubleValue())))
            .andExpect(jsonPath("$.[*].subTotal").value(hasItem(DEFAULT_SUB_TOTAL.doubleValue())));

        // Check, that the count call also returns 1
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultOrderProductLineShouldNotBeFound(String filter) throws Exception {
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingOrderProductLine() throws Exception {
        // Get the orderProductLine
        restOrderProductLineMockMvc.perform(get("/api/order-product-lines/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderProductLine() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        int databaseSizeBeforeUpdate = orderProductLineRepository.findAll().size();

        // Update the orderProductLine
        OrderProductLine updatedOrderProductLine = orderProductLineRepository.findById(orderProductLine.getId()).get();
        // Disconnect from session so that the updates on updatedOrderProductLine are not directly saved in db
        em.detach(updatedOrderProductLine);
        updatedOrderProductLine
            .quantity(UPDATED_QUANTITY)
            .subTotal(UPDATED_SUB_TOTAL);
        OrderProductLineDTO orderProductLineDTO = orderProductLineMapper.toDto(updatedOrderProductLine);

        restOrderProductLineMockMvc.perform(put("/api/order-product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProductLineDTO)))
            .andExpect(status().isOk());

        // Validate the OrderProductLine in the database
        List<OrderProductLine> orderProductLineList = orderProductLineRepository.findAll();
        assertThat(orderProductLineList).hasSize(databaseSizeBeforeUpdate);
        OrderProductLine testOrderProductLine = orderProductLineList.get(orderProductLineList.size() - 1);
        assertThat(testOrderProductLine.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testOrderProductLine.getSubTotal()).isEqualTo(UPDATED_SUB_TOTAL);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderProductLine() throws Exception {
        int databaseSizeBeforeUpdate = orderProductLineRepository.findAll().size();

        // Create the OrderProductLine
        OrderProductLineDTO orderProductLineDTO = orderProductLineMapper.toDto(orderProductLine);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderProductLineMockMvc.perform(put("/api/order-product-lines")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(orderProductLineDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OrderProductLine in the database
        List<OrderProductLine> orderProductLineList = orderProductLineRepository.findAll();
        assertThat(orderProductLineList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderProductLine() throws Exception {
        // Initialize the database
        orderProductLineRepository.saveAndFlush(orderProductLine);

        int databaseSizeBeforeDelete = orderProductLineRepository.findAll().size();

        // Delete the orderProductLine
        restOrderProductLineMockMvc.perform(delete("/api/order-product-lines/{id}", orderProductLine.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderProductLine> orderProductLineList = orderProductLineRepository.findAll();
        assertThat(orderProductLineList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
