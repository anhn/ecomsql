package com.tintinonline.ecom.web.rest;

import com.tintinonline.ecom.EcomsqlApp;
import com.tintinonline.ecom.domain.PurchaseOrder;
import com.tintinonline.ecom.domain.OrderProductLine;
import com.tintinonline.ecom.repository.PurchaseOrderRepository;
import com.tintinonline.ecom.service.PurchaseOrderService;
import com.tintinonline.ecom.service.dto.PurchaseOrderDTO;
import com.tintinonline.ecom.service.mapper.PurchaseOrderMapper;
import com.tintinonline.ecom.service.dto.PurchaseOrderCriteria;
import com.tintinonline.ecom.service.PurchaseOrderQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.tintinonline.ecom.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PurchaseOrderResource} REST controller.
 */
@SpringBootTest(classes = EcomsqlApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PurchaseOrderResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Double DEFAULT_TOTAL = 1D;
    private static final Double UPDATED_TOTAL = 2D;
    private static final Double SMALLER_TOTAL = 1D - 1D;

    private static final ZonedDateTime DEFAULT_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    private PurchaseOrderMapper purchaseOrderMapper;

    @Autowired
    private PurchaseOrderService purchaseOrderService;

    @Autowired
    private PurchaseOrderQueryService purchaseOrderQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPurchaseOrderMockMvc;

    private PurchaseOrder purchaseOrder;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchaseOrder createEntity(EntityManager em) {
        PurchaseOrder purchaseOrder = new PurchaseOrder()
            .code(DEFAULT_CODE)
            .total(DEFAULT_TOTAL)
            .created(DEFAULT_CREATED);
        return purchaseOrder;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PurchaseOrder createUpdatedEntity(EntityManager em) {
        PurchaseOrder purchaseOrder = new PurchaseOrder()
            .code(UPDATED_CODE)
            .total(UPDATED_TOTAL)
            .created(UPDATED_CREATED);
        return purchaseOrder;
    }

    @BeforeEach
    public void initTest() {
        purchaseOrder = createEntity(em);
    }

    @Test
    @Transactional
    public void createPurchaseOrder() throws Exception {
        int databaseSizeBeforeCreate = purchaseOrderRepository.findAll().size();
        // Create the PurchaseOrder
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderMapper.toDto(purchaseOrder);
        restPurchaseOrderMockMvc.perform(post("/api/purchase-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(purchaseOrderDTO)))
            .andExpect(status().isCreated());

        // Validate the PurchaseOrder in the database
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findAll();
        assertThat(purchaseOrderList).hasSize(databaseSizeBeforeCreate + 1);
        PurchaseOrder testPurchaseOrder = purchaseOrderList.get(purchaseOrderList.size() - 1);
        assertThat(testPurchaseOrder.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPurchaseOrder.getTotal()).isEqualTo(DEFAULT_TOTAL);
        assertThat(testPurchaseOrder.getCreated()).isEqualTo(DEFAULT_CREATED);
    }

    @Test
    @Transactional
    public void createPurchaseOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = purchaseOrderRepository.findAll().size();

        // Create the PurchaseOrder with an existing ID
        purchaseOrder.setId(1L);
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderMapper.toDto(purchaseOrder);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPurchaseOrderMockMvc.perform(post("/api/purchase-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(purchaseOrderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PurchaseOrder in the database
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findAll();
        assertThat(purchaseOrderList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPurchaseOrders() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purchaseOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))));
    }
    
    @Test
    @Transactional
    public void getPurchaseOrder() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get the purchaseOrder
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders/{id}", purchaseOrder.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(purchaseOrder.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.total").value(DEFAULT_TOTAL.doubleValue()))
            .andExpect(jsonPath("$.created").value(sameInstant(DEFAULT_CREATED)));
    }


    @Test
    @Transactional
    public void getPurchaseOrdersByIdFiltering() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        Long id = purchaseOrder.getId();

        defaultPurchaseOrderShouldBeFound("id.equals=" + id);
        defaultPurchaseOrderShouldNotBeFound("id.notEquals=" + id);

        defaultPurchaseOrderShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultPurchaseOrderShouldNotBeFound("id.greaterThan=" + id);

        defaultPurchaseOrderShouldBeFound("id.lessThanOrEqual=" + id);
        defaultPurchaseOrderShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllPurchaseOrdersByCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where code equals to DEFAULT_CODE
        defaultPurchaseOrderShouldBeFound("code.equals=" + DEFAULT_CODE);

        // Get all the purchaseOrderList where code equals to UPDATED_CODE
        defaultPurchaseOrderShouldNotBeFound("code.equals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where code not equals to DEFAULT_CODE
        defaultPurchaseOrderShouldNotBeFound("code.notEquals=" + DEFAULT_CODE);

        // Get all the purchaseOrderList where code not equals to UPDATED_CODE
        defaultPurchaseOrderShouldBeFound("code.notEquals=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCodeIsInShouldWork() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where code in DEFAULT_CODE or UPDATED_CODE
        defaultPurchaseOrderShouldBeFound("code.in=" + DEFAULT_CODE + "," + UPDATED_CODE);

        // Get all the purchaseOrderList where code equals to UPDATED_CODE
        defaultPurchaseOrderShouldNotBeFound("code.in=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where code is not null
        defaultPurchaseOrderShouldBeFound("code.specified=true");

        // Get all the purchaseOrderList where code is null
        defaultPurchaseOrderShouldNotBeFound("code.specified=false");
    }
                @Test
    @Transactional
    public void getAllPurchaseOrdersByCodeContainsSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where code contains DEFAULT_CODE
        defaultPurchaseOrderShouldBeFound("code.contains=" + DEFAULT_CODE);

        // Get all the purchaseOrderList where code contains UPDATED_CODE
        defaultPurchaseOrderShouldNotBeFound("code.contains=" + UPDATED_CODE);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCodeNotContainsSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where code does not contain DEFAULT_CODE
        defaultPurchaseOrderShouldNotBeFound("code.doesNotContain=" + DEFAULT_CODE);

        // Get all the purchaseOrderList where code does not contain UPDATED_CODE
        defaultPurchaseOrderShouldBeFound("code.doesNotContain=" + UPDATED_CODE);
    }


    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total equals to DEFAULT_TOTAL
        defaultPurchaseOrderShouldBeFound("total.equals=" + DEFAULT_TOTAL);

        // Get all the purchaseOrderList where total equals to UPDATED_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.equals=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsNotEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total not equals to DEFAULT_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.notEquals=" + DEFAULT_TOTAL);

        // Get all the purchaseOrderList where total not equals to UPDATED_TOTAL
        defaultPurchaseOrderShouldBeFound("total.notEquals=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsInShouldWork() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total in DEFAULT_TOTAL or UPDATED_TOTAL
        defaultPurchaseOrderShouldBeFound("total.in=" + DEFAULT_TOTAL + "," + UPDATED_TOTAL);

        // Get all the purchaseOrderList where total equals to UPDATED_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.in=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsNullOrNotNull() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total is not null
        defaultPurchaseOrderShouldBeFound("total.specified=true");

        // Get all the purchaseOrderList where total is null
        defaultPurchaseOrderShouldNotBeFound("total.specified=false");
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total is greater than or equal to DEFAULT_TOTAL
        defaultPurchaseOrderShouldBeFound("total.greaterThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the purchaseOrderList where total is greater than or equal to UPDATED_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.greaterThanOrEqual=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total is less than or equal to DEFAULT_TOTAL
        defaultPurchaseOrderShouldBeFound("total.lessThanOrEqual=" + DEFAULT_TOTAL);

        // Get all the purchaseOrderList where total is less than or equal to SMALLER_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.lessThanOrEqual=" + SMALLER_TOTAL);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsLessThanSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total is less than DEFAULT_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.lessThan=" + DEFAULT_TOTAL);

        // Get all the purchaseOrderList where total is less than UPDATED_TOTAL
        defaultPurchaseOrderShouldBeFound("total.lessThan=" + UPDATED_TOTAL);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByTotalIsGreaterThanSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where total is greater than DEFAULT_TOTAL
        defaultPurchaseOrderShouldNotBeFound("total.greaterThan=" + DEFAULT_TOTAL);

        // Get all the purchaseOrderList where total is greater than SMALLER_TOTAL
        defaultPurchaseOrderShouldBeFound("total.greaterThan=" + SMALLER_TOTAL);
    }


    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created equals to DEFAULT_CREATED
        defaultPurchaseOrderShouldBeFound("created.equals=" + DEFAULT_CREATED);

        // Get all the purchaseOrderList where created equals to UPDATED_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.equals=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created not equals to DEFAULT_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.notEquals=" + DEFAULT_CREATED);

        // Get all the purchaseOrderList where created not equals to UPDATED_CREATED
        defaultPurchaseOrderShouldBeFound("created.notEquals=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsInShouldWork() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created in DEFAULT_CREATED or UPDATED_CREATED
        defaultPurchaseOrderShouldBeFound("created.in=" + DEFAULT_CREATED + "," + UPDATED_CREATED);

        // Get all the purchaseOrderList where created equals to UPDATED_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.in=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsNullOrNotNull() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created is not null
        defaultPurchaseOrderShouldBeFound("created.specified=true");

        // Get all the purchaseOrderList where created is null
        defaultPurchaseOrderShouldNotBeFound("created.specified=false");
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created is greater than or equal to DEFAULT_CREATED
        defaultPurchaseOrderShouldBeFound("created.greaterThanOrEqual=" + DEFAULT_CREATED);

        // Get all the purchaseOrderList where created is greater than or equal to UPDATED_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.greaterThanOrEqual=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created is less than or equal to DEFAULT_CREATED
        defaultPurchaseOrderShouldBeFound("created.lessThanOrEqual=" + DEFAULT_CREATED);

        // Get all the purchaseOrderList where created is less than or equal to SMALLER_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.lessThanOrEqual=" + SMALLER_CREATED);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsLessThanSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created is less than DEFAULT_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.lessThan=" + DEFAULT_CREATED);

        // Get all the purchaseOrderList where created is less than UPDATED_CREATED
        defaultPurchaseOrderShouldBeFound("created.lessThan=" + UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void getAllPurchaseOrdersByCreatedIsGreaterThanSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        // Get all the purchaseOrderList where created is greater than DEFAULT_CREATED
        defaultPurchaseOrderShouldNotBeFound("created.greaterThan=" + DEFAULT_CREATED);

        // Get all the purchaseOrderList where created is greater than SMALLER_CREATED
        defaultPurchaseOrderShouldBeFound("created.greaterThan=" + SMALLER_CREATED);
    }


    @Test
    @Transactional
    public void getAllPurchaseOrdersByProductLineIsEqualToSomething() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);
        OrderProductLine productLine = OrderProductLineResourceIT.createEntity(em);
        em.persist(productLine);
        em.flush();
        purchaseOrder.addProductLine(productLine);
        purchaseOrderRepository.saveAndFlush(purchaseOrder);
        Long productLineId = productLine.getId();

        // Get all the purchaseOrderList where productLine equals to productLineId
        defaultPurchaseOrderShouldBeFound("productLineId.equals=" + productLineId);

        // Get all the purchaseOrderList where productLine equals to productLineId + 1
        defaultPurchaseOrderShouldNotBeFound("productLineId.equals=" + (productLineId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultPurchaseOrderShouldBeFound(String filter) throws Exception {
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(purchaseOrder.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].total").value(hasItem(DEFAULT_TOTAL.doubleValue())))
            .andExpect(jsonPath("$.[*].created").value(hasItem(sameInstant(DEFAULT_CREATED))));

        // Check, that the count call also returns 1
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultPurchaseOrderShouldNotBeFound(String filter) throws Exception {
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingPurchaseOrder() throws Exception {
        // Get the purchaseOrder
        restPurchaseOrderMockMvc.perform(get("/api/purchase-orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePurchaseOrder() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        int databaseSizeBeforeUpdate = purchaseOrderRepository.findAll().size();

        // Update the purchaseOrder
        PurchaseOrder updatedPurchaseOrder = purchaseOrderRepository.findById(purchaseOrder.getId()).get();
        // Disconnect from session so that the updates on updatedPurchaseOrder are not directly saved in db
        em.detach(updatedPurchaseOrder);
        updatedPurchaseOrder
            .code(UPDATED_CODE)
            .total(UPDATED_TOTAL)
            .created(UPDATED_CREATED);
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderMapper.toDto(updatedPurchaseOrder);

        restPurchaseOrderMockMvc.perform(put("/api/purchase-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(purchaseOrderDTO)))
            .andExpect(status().isOk());

        // Validate the PurchaseOrder in the database
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findAll();
        assertThat(purchaseOrderList).hasSize(databaseSizeBeforeUpdate);
        PurchaseOrder testPurchaseOrder = purchaseOrderList.get(purchaseOrderList.size() - 1);
        assertThat(testPurchaseOrder.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPurchaseOrder.getTotal()).isEqualTo(UPDATED_TOTAL);
        assertThat(testPurchaseOrder.getCreated()).isEqualTo(UPDATED_CREATED);
    }

    @Test
    @Transactional
    public void updateNonExistingPurchaseOrder() throws Exception {
        int databaseSizeBeforeUpdate = purchaseOrderRepository.findAll().size();

        // Create the PurchaseOrder
        PurchaseOrderDTO purchaseOrderDTO = purchaseOrderMapper.toDto(purchaseOrder);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPurchaseOrderMockMvc.perform(put("/api/purchase-orders")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(purchaseOrderDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PurchaseOrder in the database
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findAll();
        assertThat(purchaseOrderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePurchaseOrder() throws Exception {
        // Initialize the database
        purchaseOrderRepository.saveAndFlush(purchaseOrder);

        int databaseSizeBeforeDelete = purchaseOrderRepository.findAll().size();

        // Delete the purchaseOrder
        restPurchaseOrderMockMvc.perform(delete("/api/purchase-orders/{id}", purchaseOrder.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PurchaseOrder> purchaseOrderList = purchaseOrderRepository.findAll();
        assertThat(purchaseOrderList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
