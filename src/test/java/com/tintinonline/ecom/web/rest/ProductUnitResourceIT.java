package com.tintinonline.ecom.web.rest;

import com.tintinonline.ecom.EcomsqlApp;
import com.tintinonline.ecom.domain.ProductUnit;
import com.tintinonline.ecom.repository.ProductUnitRepository;
import com.tintinonline.ecom.service.ProductUnitService;
import com.tintinonline.ecom.service.dto.ProductUnitDTO;
import com.tintinonline.ecom.service.mapper.ProductUnitMapper;
import com.tintinonline.ecom.service.dto.ProductUnitCriteria;
import com.tintinonline.ecom.service.ProductUnitQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ProductUnitResource} REST controller.
 */
@SpringBootTest(classes = EcomsqlApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ProductUnitResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ProductUnitRepository productUnitRepository;

    @Autowired
    private ProductUnitMapper productUnitMapper;

    @Autowired
    private ProductUnitService productUnitService;

    @Autowired
    private ProductUnitQueryService productUnitQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProductUnitMockMvc;

    private ProductUnit productUnit;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductUnit createEntity(EntityManager em) {
        ProductUnit productUnit = new ProductUnit()
            .name(DEFAULT_NAME);
        return productUnit;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProductUnit createUpdatedEntity(EntityManager em) {
        ProductUnit productUnit = new ProductUnit()
            .name(UPDATED_NAME);
        return productUnit;
    }

    @BeforeEach
    public void initTest() {
        productUnit = createEntity(em);
    }

    @Test
    @Transactional
    public void createProductUnit() throws Exception {
        int databaseSizeBeforeCreate = productUnitRepository.findAll().size();
        // Create the ProductUnit
        ProductUnitDTO productUnitDTO = productUnitMapper.toDto(productUnit);
        restProductUnitMockMvc.perform(post("/api/product-units")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productUnitDTO)))
            .andExpect(status().isCreated());

        // Validate the ProductUnit in the database
        List<ProductUnit> productUnitList = productUnitRepository.findAll();
        assertThat(productUnitList).hasSize(databaseSizeBeforeCreate + 1);
        ProductUnit testProductUnit = productUnitList.get(productUnitList.size() - 1);
        assertThat(testProductUnit.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createProductUnitWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = productUnitRepository.findAll().size();

        // Create the ProductUnit with an existing ID
        productUnit.setId(1L);
        ProductUnitDTO productUnitDTO = productUnitMapper.toDto(productUnit);

        // An entity with an existing ID cannot be created, so this API call must fail
        restProductUnitMockMvc.perform(post("/api/product-units")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productUnitDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProductUnit in the database
        List<ProductUnit> productUnitList = productUnitRepository.findAll();
        assertThat(productUnitList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllProductUnits() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList
        restProductUnitMockMvc.perform(get("/api/product-units?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getProductUnit() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get the productUnit
        restProductUnitMockMvc.perform(get("/api/product-units/{id}", productUnit.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(productUnit.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }


    @Test
    @Transactional
    public void getProductUnitsByIdFiltering() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        Long id = productUnit.getId();

        defaultProductUnitShouldBeFound("id.equals=" + id);
        defaultProductUnitShouldNotBeFound("id.notEquals=" + id);

        defaultProductUnitShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultProductUnitShouldNotBeFound("id.greaterThan=" + id);

        defaultProductUnitShouldBeFound("id.lessThanOrEqual=" + id);
        defaultProductUnitShouldNotBeFound("id.lessThan=" + id);
    }


    @Test
    @Transactional
    public void getAllProductUnitsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList where name equals to DEFAULT_NAME
        defaultProductUnitShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the productUnitList where name equals to UPDATED_NAME
        defaultProductUnitShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProductUnitsByNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList where name not equals to DEFAULT_NAME
        defaultProductUnitShouldNotBeFound("name.notEquals=" + DEFAULT_NAME);

        // Get all the productUnitList where name not equals to UPDATED_NAME
        defaultProductUnitShouldBeFound("name.notEquals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProductUnitsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList where name in DEFAULT_NAME or UPDATED_NAME
        defaultProductUnitShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the productUnitList where name equals to UPDATED_NAME
        defaultProductUnitShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProductUnitsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList where name is not null
        defaultProductUnitShouldBeFound("name.specified=true");

        // Get all the productUnitList where name is null
        defaultProductUnitShouldNotBeFound("name.specified=false");
    }
                @Test
    @Transactional
    public void getAllProductUnitsByNameContainsSomething() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList where name contains DEFAULT_NAME
        defaultProductUnitShouldBeFound("name.contains=" + DEFAULT_NAME);

        // Get all the productUnitList where name contains UPDATED_NAME
        defaultProductUnitShouldNotBeFound("name.contains=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllProductUnitsByNameNotContainsSomething() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        // Get all the productUnitList where name does not contain DEFAULT_NAME
        defaultProductUnitShouldNotBeFound("name.doesNotContain=" + DEFAULT_NAME);

        // Get all the productUnitList where name does not contain UPDATED_NAME
        defaultProductUnitShouldBeFound("name.doesNotContain=" + UPDATED_NAME);
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultProductUnitShouldBeFound(String filter) throws Exception {
        restProductUnitMockMvc.perform(get("/api/product-units?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(productUnit.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));

        // Check, that the count call also returns 1
        restProductUnitMockMvc.perform(get("/api/product-units/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultProductUnitShouldNotBeFound(String filter) throws Exception {
        restProductUnitMockMvc.perform(get("/api/product-units?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restProductUnitMockMvc.perform(get("/api/product-units/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }

    @Test
    @Transactional
    public void getNonExistingProductUnit() throws Exception {
        // Get the productUnit
        restProductUnitMockMvc.perform(get("/api/product-units/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductUnit() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        int databaseSizeBeforeUpdate = productUnitRepository.findAll().size();

        // Update the productUnit
        ProductUnit updatedProductUnit = productUnitRepository.findById(productUnit.getId()).get();
        // Disconnect from session so that the updates on updatedProductUnit are not directly saved in db
        em.detach(updatedProductUnit);
        updatedProductUnit
            .name(UPDATED_NAME);
        ProductUnitDTO productUnitDTO = productUnitMapper.toDto(updatedProductUnit);

        restProductUnitMockMvc.perform(put("/api/product-units")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productUnitDTO)))
            .andExpect(status().isOk());

        // Validate the ProductUnit in the database
        List<ProductUnit> productUnitList = productUnitRepository.findAll();
        assertThat(productUnitList).hasSize(databaseSizeBeforeUpdate);
        ProductUnit testProductUnit = productUnitList.get(productUnitList.size() - 1);
        assertThat(testProductUnit.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingProductUnit() throws Exception {
        int databaseSizeBeforeUpdate = productUnitRepository.findAll().size();

        // Create the ProductUnit
        ProductUnitDTO productUnitDTO = productUnitMapper.toDto(productUnit);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProductUnitMockMvc.perform(put("/api/product-units")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(productUnitDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ProductUnit in the database
        List<ProductUnit> productUnitList = productUnitRepository.findAll();
        assertThat(productUnitList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteProductUnit() throws Exception {
        // Initialize the database
        productUnitRepository.saveAndFlush(productUnit);

        int databaseSizeBeforeDelete = productUnitRepository.findAll().size();

        // Delete the productUnit
        restProductUnitMockMvc.perform(delete("/api/product-units/{id}", productUnit.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProductUnit> productUnitList = productUnitRepository.findAll();
        assertThat(productUnitList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
