package com.tintinonline.ecom.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.tintinonline.ecom.web.rest.TestUtil;

public class OrderProductLineTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderProductLine.class);
        OrderProductLine orderProductLine1 = new OrderProductLine();
        orderProductLine1.setId(1L);
        OrderProductLine orderProductLine2 = new OrderProductLine();
        orderProductLine2.setId(orderProductLine1.getId());
        assertThat(orderProductLine1).isEqualTo(orderProductLine2);
        orderProductLine2.setId(2L);
        assertThat(orderProductLine1).isNotEqualTo(orderProductLine2);
        orderProductLine1.setId(null);
        assertThat(orderProductLine1).isNotEqualTo(orderProductLine2);
    }
}
