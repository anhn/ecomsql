package com.tintinonline.ecom.repository;

import com.tintinonline.ecom.domain.OrderProductLine;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the OrderProductLine entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderProductLineRepository extends JpaRepository<OrderProductLine, Long>, JpaSpecificationExecutor<OrderProductLine> {
}
