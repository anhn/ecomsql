package com.tintinonline.ecom.service;

import com.tintinonline.ecom.service.dto.ProductUnitDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.tintinonline.ecom.domain.ProductUnit}.
 */
public interface ProductUnitService {

    /**
     * Save a productUnit.
     *
     * @param productUnitDTO the entity to save.
     * @return the persisted entity.
     */
    ProductUnitDTO save(ProductUnitDTO productUnitDTO);

    /**
     * Get all the productUnits.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<ProductUnitDTO> findAll(Pageable pageable);


    /**
     * Get the "id" productUnit.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ProductUnitDTO> findOne(Long id);

    /**
     * Delete the "id" productUnit.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
