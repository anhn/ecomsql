package com.tintinonline.ecom.service;

import com.tintinonline.ecom.domain.PurchaseOrder;
import com.tintinonline.ecom.service.dto.PurchaseOrderDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.tintinonline.ecom.domain.PurchaseOrder}.
 */
public interface PurchaseOrderService {

    /**
     * Save a purchaseOrder.
     *
     * @param purchaseOrderDTO the entity to save.
     * @return the persisted entity.
     */
    PurchaseOrderDTO save(PurchaseOrderDTO purchaseOrderDTO);

    /**
     * Get all the purchaseOrders.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PurchaseOrderDTO> findAll(Pageable pageable);


    /**
     * Get the "id" purchaseOrder.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PurchaseOrderDTO> findOne(Long id);

    /**
     * Delete the "id" purchaseOrder.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     * Recalculate order parameters
     * @param purchaseOrderDTO
     * @return
     */
    public PurchaseOrderDTO calculate(PurchaseOrderDTO purchaseOrderDTO);

    /**
     * Rehydrate full order from DTO skipping orderProductLine marked delete
     * @param purchaseOrderDTO
     * @return
     */
    public PurchaseOrder rehydrate(PurchaseOrderDTO purchaseOrderDTO);
}
