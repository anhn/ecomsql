package com.tintinonline.ecom.service;

import com.tintinonline.ecom.domain.OrderProductLine;
import com.tintinonline.ecom.domain.Product;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link com.tintinonline.ecom.domain.OrderProductLine}.
 */
public interface OrderProductLineService {

    /**
     * Save a orderProductLine.
     *
     * @param orderProductLineDTO the entity to save.
     * @return the persisted entity.
     */
    OrderProductLineDTO save(OrderProductLineDTO orderProductLineDTO);

    /**
     * Get all the orderProductLines.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<OrderProductLineDTO> findAll(Pageable pageable);


    /**
     * Get the "id" orderProductLine.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OrderProductLineDTO> findOne(Long id);

    /**
     * Delete the "id" orderProductLine.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    OrderProductLine rehydrate(OrderProductLineDTO orderProductLineDTO);
}
