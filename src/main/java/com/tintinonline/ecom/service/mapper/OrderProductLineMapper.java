package com.tintinonline.ecom.service.mapper;


import com.tintinonline.ecom.domain.*;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OrderProductLine} and its DTO {@link OrderProductLineDTO}.
 */
@Mapper(componentModel = "spring", uses = {ProductMapper.class, PurchaseOrderMapper.class})
public interface OrderProductLineMapper extends EntityMapper<OrderProductLineDTO, OrderProductLine> {

    @Mapping(source = "product.id", target = "productId")
    @Mapping(source = "product.name", target = "productName")
    @Mapping(source = "purchaseOrder.id", target = "purchaseOrderId")
    @Mapping(source = "purchaseOrder.code", target = "purchaseOrderCode")
    OrderProductLineDTO toDto(OrderProductLine orderProductLine);

    @Mapping(source = "productId", target = "product")
    @Mapping(source = "purchaseOrderId", target = "purchaseOrder")
    OrderProductLine toEntity(OrderProductLineDTO orderProductLineDTO);

    default OrderProductLine fromId(Long id) {
        if (id == null) {
            return null;
        }
        OrderProductLine orderProductLine = new OrderProductLine();
        orderProductLine.setId(id);
        return orderProductLine;
    }
}
