package com.tintinonline.ecom.service.mapper;


import com.tintinonline.ecom.domain.*;
import com.tintinonline.ecom.service.dto.ProductUnitDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProductUnit} and its DTO {@link ProductUnitDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ProductUnitMapper extends EntityMapper<ProductUnitDTO, ProductUnit> {



    default ProductUnit fromId(Long id) {
        if (id == null) {
            return null;
        }
        ProductUnit productUnit = new ProductUnit();
        productUnit.setId(id);
        return productUnit;
    }
}
