package com.tintinonline.ecom.service.mapper;


import com.tintinonline.ecom.domain.*;
import com.tintinonline.ecom.service.dto.PurchaseOrderDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PurchaseOrder} and its DTO {@link PurchaseOrderDTO}.
 */
@Mapper(componentModel = "spring", uses = {OrderProductLineMapper.class})
public interface PurchaseOrderMapper extends EntityMapper<PurchaseOrderDTO, PurchaseOrder> {


    @Mapping(target = "productLines", ignore = true)
    @Mapping(target = "removeProductLine", ignore = true)
    PurchaseOrder toEntity(PurchaseOrderDTO purchaseOrderDTO);

    default PurchaseOrder fromId(Long id) {
        if (id == null) {
            return null;
        }
        PurchaseOrder purchaseOrder = new PurchaseOrder();
        purchaseOrder.setId(id);
        return purchaseOrder;
    }
}
