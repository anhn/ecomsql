package com.tintinonline.ecom.service.impl;

import com.tintinonline.ecom.service.ProductUnitService;
import com.tintinonline.ecom.domain.ProductUnit;
import com.tintinonline.ecom.repository.ProductUnitRepository;
import com.tintinonline.ecom.service.dto.ProductUnitDTO;
import com.tintinonline.ecom.service.mapper.ProductUnitMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link ProductUnit}.
 */
@Service
@Transactional
public class ProductUnitServiceImpl implements ProductUnitService {

    private final Logger log = LoggerFactory.getLogger(ProductUnitServiceImpl.class);

    private final ProductUnitRepository productUnitRepository;

    private final ProductUnitMapper productUnitMapper;

    public ProductUnitServiceImpl(ProductUnitRepository productUnitRepository, ProductUnitMapper productUnitMapper) {
        this.productUnitRepository = productUnitRepository;
        this.productUnitMapper = productUnitMapper;
    }

    @Override
    public ProductUnitDTO save(ProductUnitDTO productUnitDTO) {
        log.debug("Request to save ProductUnit : {}", productUnitDTO);
        ProductUnit productUnit = productUnitMapper.toEntity(productUnitDTO);
        productUnit = productUnitRepository.save(productUnit);
        return productUnitMapper.toDto(productUnit);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ProductUnitDTO> findAll(Pageable pageable) {
        log.debug("Request to get all ProductUnits");
        return productUnitRepository.findAll(pageable)
            .map(productUnitMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ProductUnitDTO> findOne(Long id) {
        log.debug("Request to get ProductUnit : {}", id);
        return productUnitRepository.findById(id)
            .map(productUnitMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ProductUnit : {}", id);
        productUnitRepository.deleteById(id);
    }
}
