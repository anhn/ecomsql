package com.tintinonline.ecom.service.impl;

import com.tintinonline.ecom.domain.Product;
import com.tintinonline.ecom.repository.ProductRepository;
import com.tintinonline.ecom.service.OrderProductLineService;
import com.tintinonline.ecom.domain.OrderProductLine;
import com.tintinonline.ecom.repository.OrderProductLineRepository;
import com.tintinonline.ecom.service.ProductService;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;
import com.tintinonline.ecom.service.dto.ProductDTO;
import com.tintinonline.ecom.service.mapper.OrderProductLineMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.InvalidParameterException;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderProductLine}.
 */
@Service
@Transactional
public class OrderProductLineServiceImpl implements OrderProductLineService {

    private final Logger log = LoggerFactory.getLogger(OrderProductLineServiceImpl.class);

    private final OrderProductLineRepository orderProductLineRepository;

    private final OrderProductLineMapper orderProductLineMapper;

    private final ProductService productService;
    private final ProductRepository productRepository;

    public OrderProductLineServiceImpl(
        OrderProductLineRepository orderProductLineRepository, OrderProductLineMapper orderProductLineMapper,
        ProductService productService, ProductRepository productRepository
    ) {
        this.orderProductLineRepository = orderProductLineRepository;
        this.orderProductLineMapper = orderProductLineMapper;
        this.productService = productService;
        this.productRepository = productRepository;
    }

    @Override
    public OrderProductLineDTO save(OrderProductLineDTO orderProductLineDTO) {
        log.debug("Request to save OrderProductLine : {}", orderProductLineDTO);
        OrderProductLine orderProductLine = orderProductLineMapper.toEntity(orderProductLineDTO);
        if (Boolean.TRUE.equals(orderProductLineDTO.getMarkDelete())) {
            orderProductLineRepository.delete(orderProductLine);
            return null;
        } else {
            orderProductLine = orderProductLineRepository.save(orderProductLine);
        }
        return orderProductLineMapper.toDto(orderProductLine);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<OrderProductLineDTO> findAll(Pageable pageable) {
        log.debug("Request to get all OrderProductLines");
        return orderProductLineRepository.findAll(pageable)
            .map(orderProductLineMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<OrderProductLineDTO> findOne(Long id) {
        log.debug("Request to get OrderProductLine : {}", id);
        return orderProductLineRepository.findById(id)
            .map(orderProductLineMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete OrderProductLine : {}", id);
        orderProductLineRepository.deleteById(id);
    }

    public OrderProductLine rehydrate(OrderProductLineDTO orderProductLineDTO) {
        // Validate product data
        OrderProductLine orderProductLine = orderProductLineMapper.toEntity(orderProductLineDTO);
        Optional<Product> productOpt = productRepository.findById(orderProductLineDTO.getProductId());
        if (!productOpt.isPresent()) {
            throw new InvalidParameterException("invalid product" + orderProductLineDTO.getProductId());
        }
        orderProductLine.setProduct(productOpt.get());
        return orderProductLine;
    }
}
