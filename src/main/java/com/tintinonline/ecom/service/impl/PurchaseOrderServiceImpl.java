package com.tintinonline.ecom.service.impl;

import com.tintinonline.ecom.domain.OrderProductLine;
import com.tintinonline.ecom.service.OrderProductLineService;
import com.tintinonline.ecom.service.PurchaseOrderService;
import com.tintinonline.ecom.domain.PurchaseOrder;
import com.tintinonline.ecom.repository.PurchaseOrderRepository;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;
import com.tintinonline.ecom.service.dto.PurchaseOrderDTO;
import com.tintinonline.ecom.service.mapper.PurchaseOrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Service Implementation for managing {@link PurchaseOrder}.
 */
@Service
@Transactional
public class PurchaseOrderServiceImpl implements PurchaseOrderService {

    private final Logger log = LoggerFactory.getLogger(PurchaseOrderServiceImpl.class);

    private final PurchaseOrderRepository purchaseOrderRepository;

    private final PurchaseOrderMapper purchaseOrderMapper;

    private final OrderProductLineService orderProductLineService;

    public PurchaseOrderServiceImpl(PurchaseOrderRepository purchaseOrderRepository, PurchaseOrderMapper purchaseOrderMapper,
                                    OrderProductLineService orderProductLineService
    ) {
        this.purchaseOrderRepository = purchaseOrderRepository;
        this.purchaseOrderMapper = purchaseOrderMapper;
        this.orderProductLineService = orderProductLineService;
    }

    @Override
    /**
     * Create/Update order
     * Lines to be removed must have markDelete=true
     * Automatically call calculate
     * @return updated order
     */
    public PurchaseOrderDTO save(PurchaseOrderDTO inPurchaseOrderDTO) {
        log.debug("Request to save PurchaseOrder : {}", inPurchaseOrderDTO);

        PurchaseOrder purchaseOrder = rehydrate(inPurchaseOrderDTO);
        _calculate(purchaseOrder);
        purchaseOrderRepository.save(purchaseOrder);

        purchaseOrder.getProductLines().clear();
        PurchaseOrderDTO outPurchaseOrderDTO = purchaseOrderMapper.toDto(purchaseOrder);
        Set<OrderProductLineDTO> outProductLines = outPurchaseOrderDTO.getProductLines();

        // Save/Delete product lines
        inPurchaseOrderDTO.getProductLines().forEach(line -> {
            OrderProductLineDTO lineDTO = orderProductLineService.save(line);
            if (lineDTO != null) {
                outProductLines.add(lineDTO);
            }
        });
        return outPurchaseOrderDTO;
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PurchaseOrderDTO> findAll(Pageable pageable) {
        log.debug("Request to get all PurchaseOrders");
        return purchaseOrderRepository.findAll(pageable)
            .map(purchaseOrderMapper::toDto);
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<PurchaseOrderDTO> findOne(Long id) {
        log.debug("Request to get PurchaseOrder : {}", id);
        return purchaseOrderRepository.findById(id)
            .map(purchaseOrderMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PurchaseOrder : {}", id);
        purchaseOrderRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public PurchaseOrderDTO calculate(PurchaseOrderDTO purchaseOrderDTO) {
        log.debug("Request to delete PurchaseOrder : {}", purchaseOrderDTO);
        PurchaseOrder purchaseOrder = rehydrate(purchaseOrderDTO);
        _calculate(purchaseOrder);
        PurchaseOrderDTO result = purchaseOrderMapper.toDto(purchaseOrder);

        return result;
    }

    /**
     * Rehydrate full order from DTO skipping orderProductLine marked delete
     * @param purchaseOrderDTO
     * @return
     */
    public PurchaseOrder rehydrate(PurchaseOrderDTO purchaseOrderDTO) {
        PurchaseOrder purchaseOrder = purchaseOrderMapper.toEntity(purchaseOrderDTO);
        Set<OrderProductLine> productLines = purchaseOrder.getProductLines();
        for (OrderProductLineDTO lineDTO : purchaseOrderDTO.getProductLines()) {
            OrderProductLine line = orderProductLineService.rehydrate(lineDTO);
            line.setPurchaseOrder(purchaseOrder);
            if (!Boolean.TRUE.equals(lineDTO.getMarkDelete())) {
                productLines.add(line);
            }
        }
        return purchaseOrder;
    }

    /**
     * Calculate total payment amount for the order
     * @param purchaseOrder
     * @return
     */
    PurchaseOrder _calculate(PurchaseOrder purchaseOrder) {
        double totalSum = 0;
        for (OrderProductLine line : purchaseOrder.getProductLines()) {
            try {
                double price = line.getProduct().getPrice() != null ? line.getProduct().getPrice() : 0.0;
                double quanity = line.getQuantity() != null ? line.getQuantity() : 0.0;
                totalSum += price * quanity;
            } catch (Exception e) {
            }
        }
        purchaseOrder.setTotal(totalSum);
        return purchaseOrder;
    }
}
