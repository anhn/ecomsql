package com.tintinonline.ecom.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

/**
 * A DTO for the {@link com.tintinonline.ecom.domain.Product} entity.
 */
@ApiModel(description = "The Product.")
public class ProductDTO implements Serializable {
    
    private Long id;

    /**
     * name
     */
    @ApiModelProperty(value = "name")
    private String name;

    /**
     * sku
     */
    @ApiModelProperty(value = "sku")
    private String sku;

    /**
     * description
     */
    @ApiModelProperty(value = "description")
    private String description;

    /**
     * price
     */
    @ApiModelProperty(value = "price")
    private Double price;


    private Long unitId;

    private String unitName;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long productUnitId) {
        this.unitId = productUnitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String productUnitName) {
        this.unitName = productUnitName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProductDTO)) {
            return false;
        }

        return id != null && id.equals(((ProductDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProductDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", sku='" + getSku() + "'" +
            ", description='" + getDescription() + "'" +
            ", price=" + getPrice() +
            ", unitId=" + getUnitId() +
            ", unitName='" + getUnitName() + "'" +
            "}";
    }
}
