package com.tintinonline.ecom.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * A DTO for the {@link com.tintinonline.ecom.domain.OrderProductLine} entity.
 */
@ApiModel(description = "The OrderProductLine entity.\n@author A true hipster")
public class OrderProductLineDTO implements Serializable {

    private Long id;

    /**
     * quantity
     */
    @ApiModelProperty(value = "quantity")
    private Double quantity;

    /**
     * subTotal
     */
    @ApiModelProperty(value = "subTotal")
    private Double subTotal;

    private Long productId;

    private String productName;

    private Long purchaseOrderId;

    private String purchaseOrderCode;

    private Boolean markDelete;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getPurchaseOrderId() {
        return purchaseOrderId;
    }

    public void setPurchaseOrderId(Long purchaseOrderId) {
        this.purchaseOrderId = purchaseOrderId;
    }

    public String getPurchaseOrderCode() {
        return purchaseOrderCode;
    }

    public void setPurchaseOrderCode(String purchaseOrderCode) {
        this.purchaseOrderCode = purchaseOrderCode;
    }

    public Boolean getMarkDelete() {
        return markDelete;
    }

    public void setMarkDelete(Boolean markDelete) {
        this.markDelete = markDelete;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderProductLineDTO)) {
            return false;
        }

        return id != null && id.equals(((OrderProductLineDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OrderProductLineDTO{" +
            "id=" + getId() +
            ", quantity=" + getQuantity() +
            ", subTotal=" + getSubTotal() +
            ", productId=" + getProductId() +
            ", productName='" + getProductName() + "'" +
            ", purchaseOrderId=" + getPurchaseOrderId() +
            ", purchaseOrderCode='" + getPurchaseOrderCode() + "'" +
            ", markDelete=" + getMarkDelete() +
            "}";
    }
}
