package com.tintinonline.ecom.service.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.time.ZonedDateTime;
import java.io.Serializable;
import java.util.Set;

/**
 * A DTO for the {@link com.tintinonline.ecom.domain.PurchaseOrder} entity.
 */
@ApiModel(description = "The user created purchase order.")
public class PurchaseOrderDTO implements Serializable {

    private Long id;

    /**
     * code
     */
    @ApiModelProperty(value = "code")
    private String code;

    /**
     * total
     */
    @ApiModelProperty(value = "total")
    private Double total;

    /**
     * create date
     */
    @ApiModelProperty(value = "create date")
    private ZonedDateTime created;

    @ApiModelProperty(value = "product lines")
    private Set<OrderProductLineDTO> productLines;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public Set<OrderProductLineDTO> getProductLines() {
        return productLines;
    }

    public void setProductLines(Set<OrderProductLineDTO> productLines) {
        this.productLines = productLines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchaseOrderDTO)) {
            return false;
        }

        return id != null && id.equals(((PurchaseOrderDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PurchaseOrderDTO{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", total=" + getTotal() +
            ", created='" + getCreated() + "'" +
            "}";
    }
}
