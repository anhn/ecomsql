package com.tintinonline.ecom.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.tintinonline.ecom.domain.ProductUnit;
import com.tintinonline.ecom.domain.*; // for static metamodels
import com.tintinonline.ecom.repository.ProductUnitRepository;
import com.tintinonline.ecom.service.dto.ProductUnitCriteria;
import com.tintinonline.ecom.service.dto.ProductUnitDTO;
import com.tintinonline.ecom.service.mapper.ProductUnitMapper;

/**
 * Service for executing complex queries for {@link ProductUnit} entities in the database.
 * The main input is a {@link ProductUnitCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ProductUnitDTO} or a {@link Page} of {@link ProductUnitDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ProductUnitQueryService extends QueryService<ProductUnit> {

    private final Logger log = LoggerFactory.getLogger(ProductUnitQueryService.class);

    private final ProductUnitRepository productUnitRepository;

    private final ProductUnitMapper productUnitMapper;

    public ProductUnitQueryService(ProductUnitRepository productUnitRepository, ProductUnitMapper productUnitMapper) {
        this.productUnitRepository = productUnitRepository;
        this.productUnitMapper = productUnitMapper;
    }

    /**
     * Return a {@link List} of {@link ProductUnitDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ProductUnitDTO> findByCriteria(ProductUnitCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<ProductUnit> specification = createSpecification(criteria);
        return productUnitMapper.toDto(productUnitRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ProductUnitDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ProductUnitDTO> findByCriteria(ProductUnitCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<ProductUnit> specification = createSpecification(criteria);
        return productUnitRepository.findAll(specification, page)
            .map(productUnitMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ProductUnitCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<ProductUnit> specification = createSpecification(criteria);
        return productUnitRepository.count(specification);
    }

    /**
     * Function to convert {@link ProductUnitCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<ProductUnit> createSpecification(ProductUnitCriteria criteria) {
        Specification<ProductUnit> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), ProductUnit_.id));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), ProductUnit_.name));
            }
        }
        return specification;
    }
}
