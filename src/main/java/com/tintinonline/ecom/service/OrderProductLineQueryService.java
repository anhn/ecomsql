package com.tintinonline.ecom.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.tintinonline.ecom.domain.OrderProductLine;
import com.tintinonline.ecom.domain.*; // for static metamodels
import com.tintinonline.ecom.repository.OrderProductLineRepository;
import com.tintinonline.ecom.service.dto.OrderProductLineCriteria;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;
import com.tintinonline.ecom.service.mapper.OrderProductLineMapper;

/**
 * Service for executing complex queries for {@link OrderProductLine} entities in the database.
 * The main input is a {@link OrderProductLineCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link OrderProductLineDTO} or a {@link Page} of {@link OrderProductLineDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class OrderProductLineQueryService extends QueryService<OrderProductLine> {

    private final Logger log = LoggerFactory.getLogger(OrderProductLineQueryService.class);

    private final OrderProductLineRepository orderProductLineRepository;

    private final OrderProductLineMapper orderProductLineMapper;

    public OrderProductLineQueryService(OrderProductLineRepository orderProductLineRepository, OrderProductLineMapper orderProductLineMapper) {
        this.orderProductLineRepository = orderProductLineRepository;
        this.orderProductLineMapper = orderProductLineMapper;
    }

    /**
     * Return a {@link List} of {@link OrderProductLineDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<OrderProductLineDTO> findByCriteria(OrderProductLineCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<OrderProductLine> specification = createSpecification(criteria);
        return orderProductLineMapper.toDto(orderProductLineRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link OrderProductLineDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderProductLineDTO> findByCriteria(OrderProductLineCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<OrderProductLine> specification = createSpecification(criteria);
        return orderProductLineRepository.findAll(specification, page)
            .map(orderProductLineMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(OrderProductLineCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<OrderProductLine> specification = createSpecification(criteria);
        return orderProductLineRepository.count(specification);
    }

    /**
     * Function to convert {@link OrderProductLineCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<OrderProductLine> createSpecification(OrderProductLineCriteria criteria) {
        Specification<OrderProductLine> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), OrderProductLine_.id));
            }
            if (criteria.getQuantity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getQuantity(), OrderProductLine_.quantity));
            }
            if (criteria.getSubTotal() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getSubTotal(), OrderProductLine_.subTotal));
            }
            if (criteria.getProductId() != null) {
                specification = specification.and(buildSpecification(criteria.getProductId(),
                    root -> root.join(OrderProductLine_.product, JoinType.LEFT).get(Product_.id)));
            }
            if (criteria.getPurchaseOrderId() != null) {
                specification = specification.and(buildSpecification(criteria.getPurchaseOrderId(),
                    root -> root.join(OrderProductLine_.purchaseOrder, JoinType.LEFT).get(PurchaseOrder_.id)));
            }
        }
        return specification;
    }
}
