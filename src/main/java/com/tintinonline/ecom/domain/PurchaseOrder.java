package com.tintinonline.ecom.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * The user created purchase order.
 */
@Entity
@Table(name = "purchase_order")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PurchaseOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * code
     */
    @Column(name = "code")
    private String code;

    /**
     * total
     */
    @Column(name = "total")
    private Double total;

    /**
     * create date
     */
    @Column(name = "created")
    private ZonedDateTime created;

    @OneToMany(mappedBy = "purchaseOrder")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    private Set<OrderProductLine> productLines = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public PurchaseOrder code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getTotal() {
        return total;
    }

    public PurchaseOrder total(Double total) {
        this.total = total;
        return this;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public ZonedDateTime getCreated() {
        return created;
    }

    public PurchaseOrder created(ZonedDateTime created) {
        this.created = created;
        return this;
    }

    public void setCreated(ZonedDateTime created) {
        this.created = created;
    }

    public Set<OrderProductLine> getProductLines() {
        return productLines;
    }

    public PurchaseOrder productLines(Set<OrderProductLine> orderProductLines) {
        this.productLines = orderProductLines;
        return this;
    }

    public PurchaseOrder addProductLine(OrderProductLine orderProductLine) {
        this.productLines.add(orderProductLine);
        orderProductLine.setPurchaseOrder(this);
        return this;
    }

    public PurchaseOrder removeProductLine(OrderProductLine orderProductLine) {
        this.productLines.remove(orderProductLine);
        orderProductLine.setPurchaseOrder(null);
        return this;
    }

    public void setProductLines(Set<OrderProductLine> orderProductLines) {
        this.productLines = orderProductLines;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PurchaseOrder)) {
            return false;
        }
        return id != null && id.equals(((PurchaseOrder) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PurchaseOrder{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", total=" + getTotal() +
            ", created='" + getCreated() + "'" +
            "}";
    }
}
