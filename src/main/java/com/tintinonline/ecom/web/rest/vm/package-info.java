/**
 * View Models used by Spring MVC REST controllers.
 */
package com.tintinonline.ecom.web.rest.vm;
