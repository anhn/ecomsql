package com.tintinonline.ecom.web.rest.v1;

import com.tintinonline.ecom.config.Constants;
import com.tintinonline.ecom.domain.User;
import com.tintinonline.ecom.repository.UserRepository;
import com.tintinonline.ecom.security.AuthoritiesConstants;
import com.tintinonline.ecom.service.MailService;
import com.tintinonline.ecom.service.UserService;
import com.tintinonline.ecom.service.dto.UserDTO;
import com.tintinonline.ecom.web.rest.errors.BadRequestAlertException;
import com.tintinonline.ecom.web.rest.errors.EmailAlreadyUsedException;
import com.tintinonline.ecom.web.rest.errors.LoginAlreadyUsedException;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1")
public class BaseV1Controller {
}
