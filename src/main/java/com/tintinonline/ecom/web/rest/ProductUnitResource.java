package com.tintinonline.ecom.web.rest;

import com.tintinonline.ecom.service.ProductUnitService;
import com.tintinonline.ecom.web.rest.errors.BadRequestAlertException;
import com.tintinonline.ecom.service.dto.ProductUnitDTO;
import com.tintinonline.ecom.service.dto.ProductUnitCriteria;
import com.tintinonline.ecom.service.ProductUnitQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.tintinonline.ecom.domain.ProductUnit}.
 */
@RestController
@RequestMapping("/api")
public class ProductUnitResource {

    private final Logger log = LoggerFactory.getLogger(ProductUnitResource.class);

    private static final String ENTITY_NAME = "productUnit";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ProductUnitService productUnitService;

    private final ProductUnitQueryService productUnitQueryService;

    public ProductUnitResource(ProductUnitService productUnitService, ProductUnitQueryService productUnitQueryService) {
        this.productUnitService = productUnitService;
        this.productUnitQueryService = productUnitQueryService;
    }

    /**
     * {@code POST  /product-units} : Create a new productUnit.
     *
     * @param productUnitDTO the productUnitDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new productUnitDTO, or with status {@code 400 (Bad Request)} if the productUnit has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/product-units")
    public ResponseEntity<ProductUnitDTO> createProductUnit(@RequestBody ProductUnitDTO productUnitDTO) throws URISyntaxException {
        log.debug("REST request to save ProductUnit : {}", productUnitDTO);
        if (productUnitDTO.getId() != null) {
            throw new BadRequestAlertException("A new productUnit cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProductUnitDTO result = productUnitService.save(productUnitDTO);
        return ResponseEntity.created(new URI("/api/product-units/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /product-units} : Updates an existing productUnit.
     *
     * @param productUnitDTO the productUnitDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated productUnitDTO,
     * or with status {@code 400 (Bad Request)} if the productUnitDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the productUnitDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/product-units")
    public ResponseEntity<ProductUnitDTO> updateProductUnit(@RequestBody ProductUnitDTO productUnitDTO) throws URISyntaxException {
        log.debug("REST request to update ProductUnit : {}", productUnitDTO);
        if (productUnitDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ProductUnitDTO result = productUnitService.save(productUnitDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, productUnitDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /product-units} : get all the productUnits.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of productUnits in body.
     */
    @GetMapping("/product-units")
    public ResponseEntity<List<ProductUnitDTO>> getAllProductUnits(ProductUnitCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ProductUnits by criteria: {}", criteria);
        Page<ProductUnitDTO> page = productUnitQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /product-units/count} : count all the productUnits.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/product-units/count")
    public ResponseEntity<Long> countProductUnits(ProductUnitCriteria criteria) {
        log.debug("REST request to count ProductUnits by criteria: {}", criteria);
        return ResponseEntity.ok().body(productUnitQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /product-units/:id} : get the "id" productUnit.
     *
     * @param id the id of the productUnitDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the productUnitDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/product-units/{id}")
    public ResponseEntity<ProductUnitDTO> getProductUnit(@PathVariable Long id) {
        log.debug("REST request to get ProductUnit : {}", id);
        Optional<ProductUnitDTO> productUnitDTO = productUnitService.findOne(id);
        return ResponseUtil.wrapOrNotFound(productUnitDTO);
    }

    /**
     * {@code DELETE  /product-units/:id} : delete the "id" productUnit.
     *
     * @param id the id of the productUnitDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/product-units/{id}")
    public ResponseEntity<Void> deleteProductUnit(@PathVariable Long id) {
        log.debug("REST request to delete ProductUnit : {}", id);
        productUnitService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
