package com.tintinonline.ecom.web.rest;

import com.tintinonline.ecom.service.OrderProductLineService;
import com.tintinonline.ecom.web.rest.errors.BadRequestAlertException;
import com.tintinonline.ecom.service.dto.OrderProductLineDTO;
import com.tintinonline.ecom.service.dto.OrderProductLineCriteria;
import com.tintinonline.ecom.service.OrderProductLineQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.tintinonline.ecom.domain.OrderProductLine}.
 */
@RestController
@RequestMapping("/api")
public class OrderProductLineResource {

    private final Logger log = LoggerFactory.getLogger(OrderProductLineResource.class);

    private static final String ENTITY_NAME = "orderProductLine";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderProductLineService orderProductLineService;

    private final OrderProductLineQueryService orderProductLineQueryService;

    public OrderProductLineResource(OrderProductLineService orderProductLineService, OrderProductLineQueryService orderProductLineQueryService) {
        this.orderProductLineService = orderProductLineService;
        this.orderProductLineQueryService = orderProductLineQueryService;
    }

    /**
     * {@code POST  /order-product-lines} : Create a new orderProductLine.
     *
     * @param orderProductLineDTO the orderProductLineDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderProductLineDTO, or with status {@code 400 (Bad Request)} if the orderProductLine has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-product-lines")
    public ResponseEntity<OrderProductLineDTO> createOrderProductLine(@Valid @RequestBody OrderProductLineDTO orderProductLineDTO) throws URISyntaxException {
        log.debug("REST request to save OrderProductLine : {}", orderProductLineDTO);
        if (orderProductLineDTO.getId() != null) {
            throw new BadRequestAlertException("A new orderProductLine cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderProductLineDTO result = orderProductLineService.save(orderProductLineDTO);
        return ResponseEntity.created(new URI("/api/order-product-lines/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-product-lines} : Updates an existing orderProductLine.
     *
     * @param orderProductLineDTO the orderProductLineDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderProductLineDTO,
     * or with status {@code 400 (Bad Request)} if the orderProductLineDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderProductLineDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-product-lines")
    public ResponseEntity<OrderProductLineDTO> updateOrderProductLine(@Valid @RequestBody OrderProductLineDTO orderProductLineDTO) throws URISyntaxException {
        log.debug("REST request to update OrderProductLine : {}", orderProductLineDTO);
        if (orderProductLineDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderProductLineDTO result = orderProductLineService.save(orderProductLineDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderProductLineDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /order-product-lines} : get all the orderProductLines.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderProductLines in body.
     */
    @GetMapping("/order-product-lines")
    public ResponseEntity<List<OrderProductLineDTO>> getAllOrderProductLines(OrderProductLineCriteria criteria, Pageable pageable) {
        log.debug("REST request to get OrderProductLines by criteria: {}", criteria);
        Page<OrderProductLineDTO> page = orderProductLineQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-product-lines/count} : count all the orderProductLines.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/order-product-lines/count")
    public ResponseEntity<Long> countOrderProductLines(OrderProductLineCriteria criteria) {
        log.debug("REST request to count OrderProductLines by criteria: {}", criteria);
        return ResponseEntity.ok().body(orderProductLineQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /order-product-lines/:id} : get the "id" orderProductLine.
     *
     * @param id the id of the orderProductLineDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderProductLineDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-product-lines/{id}")
    public ResponseEntity<OrderProductLineDTO> getOrderProductLine(@PathVariable Long id) {
        log.debug("REST request to get OrderProductLine : {}", id);
        Optional<OrderProductLineDTO> orderProductLineDTO = orderProductLineService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderProductLineDTO);
    }

    /**
     * {@code DELETE  /order-product-lines/:id} : delete the "id" orderProductLine.
     *
     * @param id the id of the orderProductLineDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-product-lines/{id}")
    public ResponseEntity<Void> deleteOrderProductLine(@PathVariable Long id) {
        log.debug("REST request to delete OrderProductLine : {}", id);
        orderProductLineService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
