import { Moment } from 'moment';
import { IOrderProductLine } from 'app/shared/model/order-product-line.model';

export interface IPurchaseOrder {
  id?: number;
  code?: string;
  total?: number;
  created?: string;
  productLines?: IOrderProductLine[];
}

export const defaultValue: Readonly<IPurchaseOrder> = {};
