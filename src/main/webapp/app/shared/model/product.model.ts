export interface IProduct {
  id?: number;
  name?: string;
  sku?: string;
  description?: string;
  price?: number;
  unitName?: string;
  unitId?: number;
}

export const defaultValue: Readonly<IProduct> = {};
