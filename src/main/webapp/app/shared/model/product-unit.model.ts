export interface IProductUnit {
  id?: number;
  name?: string;
}

export const defaultValue: Readonly<IProductUnit> = {};
