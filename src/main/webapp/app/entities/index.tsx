import React from 'react';
import { Switch } from 'react-router-dom';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Product from './product';
import PurchaseOrder from './purchase-order';
import OrderProductLine from './order-product-line';
import ProductUnit from './product-unit';
/* jhipster-needle-add-route-import - JHipster will add routes here */

const Routes = ({ match }) => (
  <div>
    <Switch>
      {/* prettier-ignore */}
      <ErrorBoundaryRoute path={`${match.url}product`} component={Product} />
      <ErrorBoundaryRoute path={`${match.url}purchase-order`} component={PurchaseOrder} />
      <ErrorBoundaryRoute path={`${match.url}order-product-line`} component={OrderProductLine} />
      <ErrorBoundaryRoute path={`${match.url}product-unit`} component={ProductUnit} />
      {/* jhipster-needle-add-route-path - JHipster will add routes here */}
    </Switch>
  </div>
);

export default Routes;
