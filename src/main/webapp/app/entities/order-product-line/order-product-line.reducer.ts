import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IOrderProductLine, defaultValue } from 'app/shared/model/order-product-line.model';

export const ACTION_TYPES = {
  FETCH_ORDERPRODUCTLINE_LIST: 'orderProductLine/FETCH_ORDERPRODUCTLINE_LIST',
  FETCH_ORDERPRODUCTLINE: 'orderProductLine/FETCH_ORDERPRODUCTLINE',
  CREATE_ORDERPRODUCTLINE: 'orderProductLine/CREATE_ORDERPRODUCTLINE',
  UPDATE_ORDERPRODUCTLINE: 'orderProductLine/UPDATE_ORDERPRODUCTLINE',
  DELETE_ORDERPRODUCTLINE: 'orderProductLine/DELETE_ORDERPRODUCTLINE',
  RESET: 'orderProductLine/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IOrderProductLine>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type OrderProductLineState = Readonly<typeof initialState>;

// Reducer

export default (state: OrderProductLineState = initialState, action): OrderProductLineState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_ORDERPRODUCTLINE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_ORDERPRODUCTLINE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_ORDERPRODUCTLINE):
    case REQUEST(ACTION_TYPES.UPDATE_ORDERPRODUCTLINE):
    case REQUEST(ACTION_TYPES.DELETE_ORDERPRODUCTLINE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_ORDERPRODUCTLINE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_ORDERPRODUCTLINE):
    case FAILURE(ACTION_TYPES.CREATE_ORDERPRODUCTLINE):
    case FAILURE(ACTION_TYPES.UPDATE_ORDERPRODUCTLINE):
    case FAILURE(ACTION_TYPES.DELETE_ORDERPRODUCTLINE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_ORDERPRODUCTLINE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_ORDERPRODUCTLINE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_ORDERPRODUCTLINE):
    case SUCCESS(ACTION_TYPES.UPDATE_ORDERPRODUCTLINE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_ORDERPRODUCTLINE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/order-product-lines';

// Actions

export const getEntities: ICrudGetAllAction<IOrderProductLine> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_ORDERPRODUCTLINE_LIST,
    payload: axios.get<IOrderProductLine>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IOrderProductLine> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_ORDERPRODUCTLINE,
    payload: axios.get<IOrderProductLine>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IOrderProductLine> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_ORDERPRODUCTLINE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IOrderProductLine> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_ORDERPRODUCTLINE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IOrderProductLine> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_ORDERPRODUCTLINE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
