import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './order-product-line.reducer';
import { IOrderProductLine } from 'app/shared/model/order-product-line.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IOrderProductLineDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OrderProductLineDetail = (props: IOrderProductLineDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { orderProductLineEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ecomsqlApp.orderProductLine.detail.title">OrderProductLine</Translate> [<b>{orderProductLineEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="quantity">
              <Translate contentKey="ecomsqlApp.orderProductLine.quantity">Quantity</Translate>
            </span>
            <UncontrolledTooltip target="quantity">
              <Translate contentKey="ecomsqlApp.orderProductLine.help.quantity" />
            </UncontrolledTooltip>
          </dt>
          <dd>{orderProductLineEntity.quantity}</dd>
          <dt>
            <span id="subTotal">
              <Translate contentKey="ecomsqlApp.orderProductLine.subTotal">Sub Total</Translate>
            </span>
            <UncontrolledTooltip target="subTotal">
              <Translate contentKey="ecomsqlApp.orderProductLine.help.subTotal" />
            </UncontrolledTooltip>
          </dt>
          <dd>{orderProductLineEntity.subTotal}</dd>
          <dt>
            <Translate contentKey="ecomsqlApp.orderProductLine.product">Product</Translate>
          </dt>
          <dd>{orderProductLineEntity.productName ? orderProductLineEntity.productName : ''}</dd>
          <dt>
            <Translate contentKey="ecomsqlApp.orderProductLine.purchaseOrder">Purchase Order</Translate>
          </dt>
          <dd>{orderProductLineEntity.purchaseOrderCode ? orderProductLineEntity.purchaseOrderCode : ''}</dd>
        </dl>
        <Button tag={Link} to="/order-product-line" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/order-product-line/${orderProductLineEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ orderProductLine }: IRootState) => ({
  orderProductLineEntity: orderProductLine.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OrderProductLineDetail);
