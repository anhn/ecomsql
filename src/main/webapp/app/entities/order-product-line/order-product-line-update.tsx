import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label, UncontrolledTooltip } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IProduct } from 'app/shared/model/product.model';
import { getEntities as getProducts } from 'app/entities/product/product.reducer';
import { IPurchaseOrder } from 'app/shared/model/purchase-order.model';
import { getEntities as getPurchaseOrders } from 'app/entities/purchase-order/purchase-order.reducer';
import { getEntity, updateEntity, createEntity, reset } from './order-product-line.reducer';
import { IOrderProductLine } from 'app/shared/model/order-product-line.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IOrderProductLineUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const OrderProductLineUpdate = (props: IOrderProductLineUpdateProps) => {
  const [productId, setProductId] = useState('0');
  const [purchaseOrderId, setPurchaseOrderId] = useState('0');
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { orderProductLineEntity, products, purchaseOrders, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/order-product-line' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }

    props.getProducts();
    props.getPurchaseOrders();
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...orderProductLineEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="ecomsqlApp.orderProductLine.home.createOrEditLabel">
            <Translate contentKey="ecomsqlApp.orderProductLine.home.createOrEditLabel">Create or edit a OrderProductLine</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : orderProductLineEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="order-product-line-id">
                    <Translate contentKey="global.field.id">ID</Translate>
                  </Label>
                  <AvInput id="order-product-line-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="quantityLabel" for="order-product-line-quantity">
                  <Translate contentKey="ecomsqlApp.orderProductLine.quantity">Quantity</Translate>
                </Label>
                <AvField id="order-product-line-quantity" type="string" className="form-control" name="quantity" />
                <UncontrolledTooltip target="quantityLabel">
                  <Translate contentKey="ecomsqlApp.orderProductLine.help.quantity" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label id="subTotalLabel" for="order-product-line-subTotal">
                  <Translate contentKey="ecomsqlApp.orderProductLine.subTotal">Sub Total</Translate>
                </Label>
                <AvField id="order-product-line-subTotal" type="string" className="form-control" name="subTotal" />
                <UncontrolledTooltip target="subTotalLabel">
                  <Translate contentKey="ecomsqlApp.orderProductLine.help.subTotal" />
                </UncontrolledTooltip>
              </AvGroup>
              <AvGroup>
                <Label for="order-product-line-product">
                  <Translate contentKey="ecomsqlApp.orderProductLine.product">Product</Translate>
                </Label>
                <AvInput id="order-product-line-product" type="select" className="form-control" name="productId" required>
                  {products
                    ? products.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.name}
                        </option>
                      ))
                    : null}
                </AvInput>
                <AvFeedback>
                  <Translate contentKey="entity.validation.required">This field is required.</Translate>
                </AvFeedback>
              </AvGroup>
              <AvGroup>
                <Label for="order-product-line-purchaseOrder">
                  <Translate contentKey="ecomsqlApp.orderProductLine.purchaseOrder">Purchase Order</Translate>
                </Label>
                <AvInput id="order-product-line-purchaseOrder" type="select" className="form-control" name="purchaseOrderId">
                  <option value="" key="0" />
                  {purchaseOrders
                    ? purchaseOrders.map(otherEntity => (
                        <option value={otherEntity.id} key={otherEntity.id}>
                          {otherEntity.code}
                        </option>
                      ))
                    : null}
                </AvInput>
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/order-product-line" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  products: storeState.product.entities,
  purchaseOrders: storeState.purchaseOrder.entities,
  orderProductLineEntity: storeState.orderProductLine.entity,
  loading: storeState.orderProductLine.loading,
  updating: storeState.orderProductLine.updating,
  updateSuccess: storeState.orderProductLine.updateSuccess,
});

const mapDispatchToProps = {
  getProducts,
  getPurchaseOrders,
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(OrderProductLineUpdate);
