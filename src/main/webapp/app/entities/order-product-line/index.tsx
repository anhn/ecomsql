import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import OrderProductLine from './order-product-line';
import OrderProductLineDetail from './order-product-line-detail';
import OrderProductLineUpdate from './order-product-line-update';
import OrderProductLineDeleteDialog from './order-product-line-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={OrderProductLineUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={OrderProductLineUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={OrderProductLineDetail} />
      <ErrorBoundaryRoute path={match.url} component={OrderProductLine} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={OrderProductLineDeleteDialog} />
  </>
);

export default Routes;
