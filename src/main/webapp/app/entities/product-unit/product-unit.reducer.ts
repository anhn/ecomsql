import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IProductUnit, defaultValue } from 'app/shared/model/product-unit.model';

export const ACTION_TYPES = {
  FETCH_PRODUCTUNIT_LIST: 'productUnit/FETCH_PRODUCTUNIT_LIST',
  FETCH_PRODUCTUNIT: 'productUnit/FETCH_PRODUCTUNIT',
  CREATE_PRODUCTUNIT: 'productUnit/CREATE_PRODUCTUNIT',
  UPDATE_PRODUCTUNIT: 'productUnit/UPDATE_PRODUCTUNIT',
  DELETE_PRODUCTUNIT: 'productUnit/DELETE_PRODUCTUNIT',
  RESET: 'productUnit/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IProductUnit>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type ProductUnitState = Readonly<typeof initialState>;

// Reducer

export default (state: ProductUnitState = initialState, action): ProductUnitState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTUNIT_LIST):
    case REQUEST(ACTION_TYPES.FETCH_PRODUCTUNIT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_PRODUCTUNIT):
    case REQUEST(ACTION_TYPES.UPDATE_PRODUCTUNIT):
    case REQUEST(ACTION_TYPES.DELETE_PRODUCTUNIT):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTUNIT_LIST):
    case FAILURE(ACTION_TYPES.FETCH_PRODUCTUNIT):
    case FAILURE(ACTION_TYPES.CREATE_PRODUCTUNIT):
    case FAILURE(ACTION_TYPES.UPDATE_PRODUCTUNIT):
    case FAILURE(ACTION_TYPES.DELETE_PRODUCTUNIT):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTUNIT_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_PRODUCTUNIT):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_PRODUCTUNIT):
    case SUCCESS(ACTION_TYPES.UPDATE_PRODUCTUNIT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_PRODUCTUNIT):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/product-units';

// Actions

export const getEntities: ICrudGetAllAction<IProductUnit> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTUNIT_LIST,
    payload: axios.get<IProductUnit>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<IProductUnit> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_PRODUCTUNIT,
    payload: axios.get<IProductUnit>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<IProductUnit> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_PRODUCTUNIT,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IProductUnit> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_PRODUCTUNIT,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<IProductUnit> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_PRODUCTUNIT,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
