import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, UncontrolledTooltip, Row, Col } from 'reactstrap';
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './product.reducer';
import { IProduct } from 'app/shared/model/product.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IProductDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const ProductDetail = (props: IProductDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { productEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          <Translate contentKey="ecomsqlApp.product.detail.title">Product</Translate> [<b>{productEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="name">
              <Translate contentKey="ecomsqlApp.product.name">Name</Translate>
            </span>
            <UncontrolledTooltip target="name">
              <Translate contentKey="ecomsqlApp.product.help.name" />
            </UncontrolledTooltip>
          </dt>
          <dd>{productEntity.name}</dd>
          <dt>
            <span id="sku">
              <Translate contentKey="ecomsqlApp.product.sku">Sku</Translate>
            </span>
            <UncontrolledTooltip target="sku">
              <Translate contentKey="ecomsqlApp.product.help.sku" />
            </UncontrolledTooltip>
          </dt>
          <dd>{productEntity.sku}</dd>
          <dt>
            <span id="description">
              <Translate contentKey="ecomsqlApp.product.description">Description</Translate>
            </span>
            <UncontrolledTooltip target="description">
              <Translate contentKey="ecomsqlApp.product.help.description" />
            </UncontrolledTooltip>
          </dt>
          <dd>{productEntity.description}</dd>
          <dt>
            <span id="price">
              <Translate contentKey="ecomsqlApp.product.price">Price</Translate>
            </span>
            <UncontrolledTooltip target="price">
              <Translate contentKey="ecomsqlApp.product.help.price" />
            </UncontrolledTooltip>
          </dt>
          <dd>{productEntity.price}</dd>
          <dt>
            <Translate contentKey="ecomsqlApp.product.unit">Unit</Translate>
          </dt>
          <dd>{productEntity.unitName ? productEntity.unitName : ''}</dd>
        </dl>
        <Button tag={Link} to="/product" replace color="info">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/product/${productEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ product }: IRootState) => ({
  productEntity: product.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetail);
